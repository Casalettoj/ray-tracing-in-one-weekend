use thiserror::Error;

use crate::primitives::rgb_pixel::RgbChannel;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Channel {0:?} too large. Attempted values: {1}, Max value: {2}.")]
    ChannelValueTooLarge(RgbChannel, f64, f64),
    #[error("Channel {0:?} too small. Attempted values: {1}, Min value: {2}.")]
    ChannelValueTooSmall(RgbChannel, f64, f64),
}
