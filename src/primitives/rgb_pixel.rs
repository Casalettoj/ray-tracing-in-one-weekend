// TODO completely refactor not happy at all
use derive_more::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

use crate::primitives::Vec3;

pub mod errors;
use crate::primitives::color::Color;
use errors::{Error, Result};

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum RgbChannel {
    R,
    G,
    B,
}

#[derive(
    Default,
    Debug,
    Copy,
    Clone,
    PartialEq,
    Add,
    Sub,
    AddAssign,
    SubAssign,
    Neg,
    Mul,
    Div,
    MulAssign,
    DivAssign,
)]
pub struct RgbPixel(Vec3<f64>);

impl RgbPixel {
    const U8_MAX_COUNT: f64 = 256.0;
    const NORMALIZED_VALUE_MIN: f64 = 0.0;
    const NORMALIZED_VALUE_MAX: f64 = 1.0;

    pub fn try_new(r: f64, g: f64, b: f64) -> Result<Self> {
        Self::validate_channel(RgbChannel::R, r)?;
        Self::validate_channel(RgbChannel::G, g)?;
        Self::validate_channel(RgbChannel::B, b)?;
        Ok(Self(Vec3::new(r, g, b)))
    }

    fn validate_channel(channel_name: RgbChannel, c: f64) -> Result<()> {
        (Self::NORMALIZED_VALUE_MIN..=Self::NORMALIZED_VALUE_MAX)
            .contains(&c)
            .then(|| {})
            .ok_or_else(|| {
                if c.lt(&Self::NORMALIZED_VALUE_MAX) {
                    Error::ChannelValueTooSmall(channel_name, c, Self::NORMALIZED_VALUE_MIN)
                } else {
                    Error::ChannelValueTooLarge(channel_name, c, Self::NORMALIZED_VALUE_MAX)
                }
            })
    }

    pub fn byte_values(&self) -> (u8, u8, u8) {
        (
            (self.r().clamp(0.0, 0.999) * Self::U8_MAX_COUNT) as u8,
            (self.g().clamp(0.0, 0.999) * Self::U8_MAX_COUNT) as u8,
            (self.b().clamp(0.0, 0.999) * Self::U8_MAX_COUNT) as u8,
        )
    }

    pub fn r(&self) -> f64 {
        self.0.x()
    }

    pub fn g(&self) -> f64 {
        self.0.y()
    }

    pub fn b(&self) -> f64 {
        self.0.z()
    }

    pub fn channel(&self, channel: RgbChannel) -> f64 {
        match channel {
            RgbChannel::R => self.r(),
            RgbChannel::G => self.g(),
            RgbChannel::B => self.b(),
        }
    }
}

impl From<Color> for RgbPixel {
    fn from(c: Color) -> Self {
        let r = c.x();
        let g = c.y();
        let b = c.z();
        Self::try_new(r, g, b).unwrap()
    }
}
