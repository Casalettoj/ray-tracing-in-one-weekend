use crate::primitives::{point3::Point3, Vec3};

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    origin: Point3,
    direction: Vec3<f64>,
}

impl Ray {
    pub fn new(origin: Point3, direction: Vec3<f64>) -> Self {
        Ray { origin, direction }
    }

    pub fn origin(&self) -> Point3 {
        self.origin
    }

    pub fn direction(&self) -> Vec3<f64> {
        self.direction
    }

    pub fn at(&self, t: f64) -> Point3 {
        self.origin + (self.direction * t)
    }
}
