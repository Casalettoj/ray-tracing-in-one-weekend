use std::ops::{Mul, Neg, Not};

use crate::rendering::material::Material;
use crate::rendering::scatter::{Scatter, ScatterResult};
use crate::{
    primitives::{point3::Point3, ray::Ray},
    raytracing::{hit::Hit, hit_result::HitResult, raytrace_collider::RaytraceCollider},
    shape::sphere::Sphere,
};

#[derive(Debug, Copy, Clone)]
pub struct RaytraceSphere {
    sphere: Sphere,
    material: Material,
}

impl RaytraceSphere {
    pub fn new(sphere: Sphere, material: Material) -> Self {
        Self { sphere, material }
    }
}

impl RaytraceCollider for RaytraceSphere {}

impl Scatter for RaytraceSphere {
    fn scatter(&self, ray: &Ray, hit_result: HitResult) -> Option<ScatterResult> {
        self.material.scatter(ray, hit_result)
    }
}

impl Hit for RaytraceSphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitResult> {
        let o_hr = self.sphere.hit(r, t_min, t_max);
        match o_hr {
            None => None,
            Some(mut hr) => {
                hr.update_material(Some(self.material));
                Some(hr)
            }
        }
    }
}

impl Hit for Sphere {
    // TODO refactor
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitResult> {
        let oc: Point3 = r.origin() - self.center();
        let a = r.direction().magnitude().mul(r.direction().magnitude());
        let half_b = oc.dot_product(&r.direction());
        let c = oc
            .magnitude()
            .mul_add(oc.magnitude(), (self.radius().mul(self.radius())).neg());
        let discriminant = half_b * half_b - a * c;

        if discriminant < 0.0 {
            return None;
        }

        let sqrt_d = discriminant.sqrt();

        // Find the nearest root that lies in the acceptable range
        let mut root = (-half_b - sqrt_d) / a;
        if (t_min..=t_max).contains(&root).not() {
            root = (-half_b + sqrt_d) / a;
            if (t_min..=t_max).contains(&root).not() {
                return None;
            }
        }

        let t = root;
        let point = r.at(t);
        let outward_normal = (point - self.center()) / self.radius();
        Some(HitResult::new(r, point, outward_normal, t, None))
    }
}
