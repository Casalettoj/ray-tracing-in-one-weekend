use crate::primitives::point3::Point3;
use crate::primitives::Vec3;
use num_traits::{FloatConst, Zero};
use rand::{thread_rng, Rng};
use std::ops::{Div, Mul};

pub const NEAR_ZERO_THRESHOLD: f64 = 1e-8;

pub fn degrees_to_radians(degrees: f64) -> f64 {
    degrees.mul(f64::PI()).div(180.0)
}

pub fn random_point(min: f64, max: f64) -> Point3 {
    let mut r = thread_rng();
    Point3::new(
        r.gen_range(min..=max),
        r.gen_range(min..=max),
        r.gen_range(min..=max),
    )
}

pub fn random_in_unit_sphere() -> Point3 {
    let mut p = random_point(-1.0, 1.0);
    loop {
        if p.magnitude().mul(p.magnitude()).lt(&1.0) {
            break;
        }
        p = random_point(-1.0, 1.0)
    }
    p
}

pub fn random_in_hemisphere(normal: Vec3<f64>) -> Vec3<f64> {
    let in_unit_sphere = random_in_unit_sphere();
    if in_unit_sphere.dot_product(&normal).gt(&f64::zero()) {
        // In the same hemisphere as the normal
        in_unit_sphere
    } else {
        -in_unit_sphere
    }
}

pub fn is_vec3_near_zero(vec3: Vec3<f64>) -> bool {
    vec3.x().abs().lt(&NEAR_ZERO_THRESHOLD)
        && vec3.y().abs().lt(&NEAR_ZERO_THRESHOLD)
        && vec3.z().abs().lt(&NEAR_ZERO_THRESHOLD)
}

pub fn random_in_unit_disk() -> Point3 {
    let mut rng = thread_rng();
    loop {
        let p = Point3::new(rng.gen_range(-1.0..=1.0), rng.gen_range(-1.0..=1.0), 0.0);
        if p.magnitude().mul(p.magnitude()).ge(&1.0) {
            continue;
        };
        break p;
    }
}
