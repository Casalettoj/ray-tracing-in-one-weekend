use num_traits::Pow;

use crate::{
    primitives::{color::Color, ray::Ray},
    raytracing::hit_result::HitResult,
    rendering::scatter::{Scatter, ScatterResult},
};
use rand::{thread_rng, Rng};

#[derive(Debug, Copy, Clone)]
pub struct DielectricMaterial(f64);

impl DielectricMaterial {
    pub fn new(refraction_index: f64) -> Self {
        Self(refraction_index)
    }

    pub fn refraction_index(&self) -> f64 {
        self.0
    }

    pub fn set_refraction_index(&mut self, new_refraction_index: f64) {
        self.0 = new_refraction_index
    }

    fn schlick_approx_reflectance(&self, cosine: f64, refraction_index: f64) -> f64 {
        let mut r0 = (1.0 - refraction_index) / (1.0 + refraction_index);
        r0 = r0 * r0;
        r0 + ((1.0 - r0) * f64::pow(1.0 - cosine, 5))
    }
}

// TODO lol
impl Scatter for DielectricMaterial {
    fn scatter(&self, r_in: &Ray, hit_result: HitResult) -> Option<ScatterResult> {
        let refraction_ratio = hit_result
            .front_face()
            .then(|| 1.0 / self.refraction_index())
            .unwrap_or(self.refraction_index());

        let unit_direction = r_in.direction().unit_vector().unwrap(); // TODO

        let cos_theta = ((-unit_direction).dot_product(&hit_result.normal())).min(1.0);
        let sin_theta = (1.0 - (cos_theta * cos_theta)).sqrt();

        let cannot_refract = refraction_ratio * sin_theta > 1.0;
        let reflectance = self.schlick_approx_reflectance(cos_theta, refraction_ratio)
            > thread_rng().gen_range(0.0..1.0);

        if cannot_refract || reflectance {
            let direction = unit_direction.reflect(&hit_result.normal());
            let scattered_ray = Ray::new(hit_result.point(), direction);
            Some(ScatterResult::new(scattered_ray, Color::new(1.0, 1.0, 1.0)))
        } else {
            let direction = unit_direction.refract(&hit_result.normal(), refraction_ratio);
            let scattered_ray = Ray::new(hit_result.point(), direction);
            Some(ScatterResult::new(scattered_ray, Color::new(1.0, 1.0, 1.0)))
        }
    }
}
