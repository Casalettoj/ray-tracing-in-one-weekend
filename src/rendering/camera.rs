use std::ops::{Div, Mul};

use crate::math::{degrees_to_radians, random_in_unit_sphere};
use crate::primitives::aspect_ratio::AspectRatio;
use crate::primitives::{point3::Point3, ray::Ray, Vec3};

#[derive(Debug, Copy, Clone)]
pub struct Camera {
    origin: Point3,
    horizontal: Vec3<f64>,
    vertical: Vec3<f64>,
    lower_left_corner: Point3,
    v_fov_deg: f64,
    aspect_ratio: AspectRatio,
    lens_radius: f64,
    u: Vec3<f64>,
    v: Vec3<f64>,
    w: Vec3<f64>,
}

impl Camera {
    pub fn new(
        v_fov_deg: f64,
        aspect_ratio: AspectRatio,
        look_from: Point3,
        look_at: Point3,
        v_up: Vec3<f64>,
        aperture: f64,
        focus_distance: f64,
    ) -> Self {
        let (viewport_width, viewport_height) =
            Self::calculate_viewport_x_y_dimensions(v_fov_deg, aspect_ratio);

        let w: Vec3<f64> = (look_from - look_at).unit_vector().unwrap(); // TODO - all these
        let u = v_up.cross_product(&w).unit_vector().unwrap();
        let v = w.cross_product(&u);

        let origin = look_from;

        let horizontal = u * viewport_width * focus_distance;
        let vertical = v * viewport_height * focus_distance;

        let lower_left_corner =
            origin - (horizontal / 2.0) - (vertical / 2.0) - (w * focus_distance);

        let lens_radius = aperture / 2.0;

        Self {
            origin,
            horizontal,
            vertical,
            lower_left_corner,
            v_fov_deg,
            aspect_ratio,
            lens_radius,
            u,
            v,
            w,
        }
    }

    pub fn origin(&self) -> Point3 {
        self.origin
    }

    pub fn lens_radius(&self) -> f64 {
        self.lens_radius
    }

    pub fn horizontal(&self) -> Vec3<f64> {
        self.horizontal
    }

    pub fn vertical(&self) -> Vec3<f64> {
        self.vertical
    }

    pub fn u(&self) -> Vec3<f64> {
        self.u
    }

    pub fn v(&self) -> Vec3<f64> {
        self.v
    }

    pub fn w(&self) -> Vec3<f64> {
        self.w
    }

    pub fn v_fov(&self) -> f64 {
        self.v_fov_deg
    }

    pub fn aspect_ratio(&self) -> AspectRatio {
        self.aspect_ratio
    }

    pub fn lower_left_corner(&self) -> Point3 {
        self.lower_left_corner
    }

    pub fn get_ray(&self, s: f64, t: f64) -> Ray {
        let rd = random_in_unit_sphere() * self.lens_radius();
        let offset = (self.u() * rd.x()) + (self.v() * rd.y());

        Ray::new(
            self.origin + offset,
            self.lower_left_corner + (self.horizontal.mul(s)) + (self.vertical.mul(t))
                - self.origin
                - offset,
        )
    }

    fn calculate_viewport_x_y_dimensions(v_fov: f64, aspect_ratio: AspectRatio) -> (f64, f64) {
        let theta = degrees_to_radians(v_fov);
        let h = theta.div(2.0).tan();
        let viewport_height = h.mul(2.0);
        let viewport_width = aspect_ratio.calculate_width(viewport_height);

        (viewport_width, viewport_height)
    }
}

// TODO tests
