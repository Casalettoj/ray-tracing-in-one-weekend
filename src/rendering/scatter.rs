use crate::primitives::color::Color;
use crate::primitives::ray::Ray;
use crate::raytracing::hit_result::HitResult;

pub trait Scatter {
    fn scatter(&self, ray: &Ray, hit_result: HitResult) -> Option<ScatterResult>;
}

#[derive(Debug, Copy, Clone)]
pub struct ScatterResult {
    ray: Ray,
    attenuation: Color,
}

impl ScatterResult {
    pub fn new(ray: Ray, attenuation: Color) -> Self {
        Self { ray, attenuation }
    }

    pub fn ray(&self) -> Ray {
        self.ray
    }

    pub fn attenuation(&self) -> Color {
        self.attenuation
    }
}
