use crate::{
    primitives::{color::Color, ray::Ray},
    raytracing::hit_result::HitResult,
    rendering::{
        material::{
            dielectric::DielectricMaterial, lambertian::LambertianMaterial, metal::MetalMaterial,
        },
        scatter::{Scatter, ScatterResult},
    },
};

pub mod dielectric;
pub mod lambertian;
pub mod metal;

#[derive(Debug, Copy, Clone)]
pub enum Material {
    Lambertian(LambertianMaterial),
    Metal(MetalMaterial),
    Dielectric(DielectricMaterial),
}

impl Material {
    pub fn new_lambertian(albedo: Color) -> Self {
        Material::Lambertian(LambertianMaterial::new(albedo))
    }

    pub fn new_metal(albedo: Color, fuzz: f64) -> Self {
        Material::Metal(MetalMaterial::new(albedo, fuzz))
    }

    pub fn new_dielectric(refraction_index: f64) -> Self {
        Material::Dielectric(DielectricMaterial::new(refraction_index))
    }
}

impl Scatter for Material {
    fn scatter(&self, ray: &Ray, hit_result: HitResult) -> Option<ScatterResult> {
        match self {
            Self::Lambertian(l) => l.scatter(ray, hit_result),
            Self::Metal(m) => m.scatter(ray, hit_result),
            Self::Dielectric(d) => d.scatter(ray, hit_result),
        }
    }
}
