use crate::primitives::aspect_ratio::AspectRatio;
use crate::primitives::ray::Ray;
use crate::raytracing::hit_result::HitResult;
use crate::raytracing::raytrace_collider::RaytraceCollider;

pub struct Scene {
    height: f64,
    width: f64,
    objects: Vec<Box<dyn RaytraceCollider>>,
}

impl Scene {
    pub fn new(
        width: f64,
        aspect_ratio: AspectRatio,
        objects: Vec<Box<dyn RaytraceCollider>>,
    ) -> Self {
        Self {
            height: aspect_ratio.calculate_height(width),
            width,
            objects,
        }
    }

    pub fn w(&self) -> f64 {
        self.width
    }

    pub fn h(&self) -> f64 {
        self.height
    }

    pub fn objects(&self) -> &Vec<Box<dyn RaytraceCollider>> {
        &self.objects
    }

    pub fn cast_ray(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitResult> {
        let mut closest_so_far = t_max;
        let mut top_hit = None;
        for hittable_obj in self.objects() {
            let hit_result = hittable_obj.hit(r, t_min, closest_so_far);
            match hit_result {
                None => continue,
                Some(hit) => {
                    closest_so_far = hit.t();
                    top_hit = Some(hit);
                }
            }
        }

        top_hit
    }
}
